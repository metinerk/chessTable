import java.util.*;

public class Main {

    //to calculate L shaped movements of knights
    private int row[] = { 2, 2, -2, -2, 1, 1, -1, -1 };
    private int col[] = { -1, 1, 1, -1, 2, -2, 2, -2 };

    //chess table, two dim array.
    static private int[][] chessTable = new int[8][8];

    static class Node{

        int magnitude;
        int x;
        int y;

        Node(){};

        Node(int x,int y){
            this.x = x;
            this.y = y;
        }

        public boolean isEqual(Node node){
            return node.x == x && node.y == y;
        }

    }




    public void fillChess(){

        int numberToFill = 0;
        for(int i=0;i<chessTable.length;i++){

            for(int j=0;j<chessTable.length;j++){

                chessTable[i][j] = numberToFill;
                numberToFill++;
            }
        }

    }

    public void viewChessTable(){
        for(int i = 0;i<8;i++){
            for(int j=0;j<8;j++){


                if(i == 0)
                    System.out.print("| ");
                else if(i==1 && j<2){
                    System.out.print("| ");
                }
                else{
                    System.out.print("|");

                }
                System.out.print(chessTable[i][j]);
            }
            System.out.println();
        }

    }

    public int breadthFirstSearch(Node src, Node dest){

        // to store calculated nodes.
        Map<Node,Boolean> visited = new HashMap<> ();
        // queue for algorithm
        Queue<Node> queue = new LinkedList<>();

        queue.add(src);

        while (!queue.isEmpty()){
            Node node = queue.remove();

            int x = node.x;
            int y = node.y;
            int magnitude = node.magnitude;

            //if algorith calculates for destination magnitude, it means knight arrived
            if (node.isEqual(dest))
                return magnitude;


            //calculate the next node if it is not already calculated.
            if (visited.get(node) == null)
            {
                visited.put(node, false);
            }

            // puts reachable nodes to the queue.
            if (!visited.get(node)) {
                visited.put(node, true);

                for (int i = 0; i < 8; i++) {
                    int x1 = x + row[i];
                    int y1 = y + col[i];
                    if (isPositionReachable(x1, y1)) {
                        Node n = new Node();
                        n.x = x1;
                        n.y = y1;
                        n.magnitude = magnitude+ 1;
                        queue.add(n);
                    }

                }
            }


        }
        return -1;
    }


    // returns false if destination is not on the chess table.
    private boolean isPositionReachable(int x, int y){
        if(x < 0 || y < 0 || x >= 8 || y >= 8)
            return false;
        return true;
    }



    //returns an array filled with index of given value
    public int[] findIndexOfValue(int value) {


        int[] indexes = new int[2];

        for (int i = 0; i < chessTable.length; i++) {
            for (int j = 0; j < chessTable.length; j++) {
                if (value == chessTable[i][j]){
                    indexes[0] = i;
                    indexes[1] = j;
                }
            }
        }

        return indexes;
    }

    public static void main(String[] args) {


        while (true){

        int src;
        int dest;

        int srcIndexes[];
        int destIndexes[];

        Node source;
        Node destination;

        Main game = new Main();
        game.fillChess();
        System.out.println("----------------------------------");
        game.viewChessTable();

        Scanner reader = new Scanner(System.in);


        do{

            System.out.println("Destination and starting point must be 0 <= x <= 63");
            System.out.println("Enter a starting point: ");
            src= reader.nextInt();
            System.out.println("Enter a destination point: ");
            dest = reader.nextInt();

            srcIndexes = game.findIndexOfValue(src);
            destIndexes = game.findIndexOfValue(dest);


             source = new Node(srcIndexes[0],srcIndexes[1]);
             destination = new Node(destIndexes[0],destIndexes[1]);



        }while (dest > 63 || dest < 0 || src < 0 || src > 63);

            System.out.println("The number of moves that knight did : " + game.breadthFirstSearch(source,destination));
        }

    }
}
